import React from 'react';
import ReactDOM from 'react-dom';
import fetch from 'isomorphic-fetch';


let TipoSelect = function(props) {
    return (
        <div>
            <label htmlFor="tipo_id" className="control-label">Tipo da ocorrência</label>
            <select id="tipo_id" name={props.htmlName} className={props.classes} onChange={props.handleChange}>
                <option>---------</option>
                <option value="1">Reclamação</option>
                <option value="2">Solicitação</option>
                <option value="3">Informação</option>
                <option value="4">Elogio</option>
                <option value="5">Sugestão</option>
                <option value="6">Denúncia</option>
                <option value="7">Manifesto</option>
            </select>
        </div>
    )
}

let CategoriaSelect = function(props) {
    return (
        <div>
            <label htmlFor="id_categoria" className="control-label">Categoria da ocorrência</label>
            <select id="id_categoria" name={props.htmlName} className={props.classes} onChange={props.handleChange}>
                <option>---------</option>
                <option value="3">Trânsito</option>
                <option value="4">Transporte</option>
            </select>
        </div>
    )
}

let TipoTransporteSelect = function(props) {
    return (
        <div>
            <label htmlFor="id_tipo_transporte" className="control-label">Tipo do transporte</label>
            <select id="id_tipo_transporte" name={props.htmlName} className={props.classes} onChange={props.handleChange}>
              <option>---------</option>
              <option value="1">ONIBUS</option>
              <option value="2">TAXI</option>
              <option value="3">ESCOLAR</option>
              <option value="4">TURISMO</option>
            </select>
        </div>
    )
}

let MotivoSelect = function(props) {
    return (
        <div>
          <label htmlFor="id_motivo" className="control-label">Motivo</label>
            <select id="id_motivo" name={props.htmlName} className={props.classes} required>
                <option>---------</option>
                {props.motivos.map( motivo => <option value={motivo.id} key={motivo.id}>{motivo.descricao}</option>)}
            </select>
        </div>
    )
};

const CATEGORIA_TRANSITO_VALUE = 3;

export default class App extends React.Component {

    state = {
        isLoading: true,
        tipoSelected: 1,
        categoriaSelected: 3,
        transporteSelected: 0,
        motivos: []
    }

    constructor(props) {
        super(props);
        this.fetchMotivos = this.fetchMotivos.bind(this);
    }

    fetchMotivos() {
        fetch(`/cerin/api/motivos/tipos/${this.state.tipoSelected}/categorias/${this.state.categoriaSelected}/transportes/${this.state.transporteSelected}/`)
        .then((response) => response.json())
        .then((json) => {
            this.setState({
                motivos: json,
                isLoading: false
            })
        })
        .catch((error) => {
            this.setState({
                motivos: [],
                isLoading: false
            })
        })
    }

    handleTipo = (event) => {
        // handle on change at tipo select
        this.setState({
            tipoSelected: parseInt(event.target.value)
        }, this.fetchMotivos)

    }

    handleCategoria = (event) => {
        this.setState({
            categoriaSelected: parseInt(event.target.value)
        }, () => {
          // Se a categoria for igual a transito então o transporte é zero
          // e fazemos o fetch dos motivos, caso contrário zeramos os motivos
          if (this.state.categoriaSelected === CATEGORIA_TRANSITO_VALUE) {
            this.setState({
                transporteSelected: 0
            }, this.fetchMotivos)
          } else {
            this.setState({
                motivos: []
            })
          }
        })
    }

    handleTransporte = (event) => {
        this.setState({
            transporteSelected: (event.target.value !== undefined && event.target.value > 0) ? parseInt(event.target.value) : 0
        }, this.fetchMotivos);
    }

    render() {
        return (
            <div className="form-row">
                <div className="col-4">
                  <TipoSelect classes="form-control" htmlName="tipo" handleChange={this.handleTipo} />
                </div>
                <div className="col-4">
                  <CategoriaSelect classes="form-control" htmlName="categoria" handleChange={this.handleCategoria} />
                </div>
                <div className="col-4">
                  <TipoTransporteSelect classes="form-control" htmlName="tipo_transporte" handleChange={this.handleTransporte}/>
                </div>
                <div className="col-12">
                  <MotivoSelect classes="form-control" htmlName="motivo" motivos={this.state.motivos} />
                </div>
            </div>
        )
    }
};

let removeHtmlFields = (fields) => {
  for (var i = 0; i < fields.length; i++) {
    let documentFields = document.getElementsByName(fields[i]);

    for (var j = 0; j < documentFields.length; j++) {
      documentFields[j].parentElement.parentElement.remove();
    }
  }
}

document.addEventListener("DOMContentLoaded", function() {
    var input         = document.getElementsByName("motivo")[0],
        parent        = input.parentElement;

    if (parent !== undefined && !!parent) {
      removeHtmlFields(["tipo", "categoria", "tipo_transporte"])
      ReactDOM.render(<App />, parent);
    }
});
