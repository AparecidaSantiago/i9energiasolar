"""energiaSolar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from .apps.authorization import views
from .apps.core.views import home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', login_required(home), name="root_path" ),
    url(r'^core/', include('energiaSolar.apps.core.urls', namespace='core')),
    url(r'^auth/',
        include('energiaSolar.apps.authorization.urls', namespace='authorization')),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^clientes/',
        include('energiaSolar.apps.clientes.urls', namespace='clientes')),
    url(r'^produtos/',
        include('energiaSolar.apps.produtos.urls', namespace='produtos')),
    url(r'^administracao/',
         include('energiaSolar.apps.administracao.urls', namespace='administracao')),
    url(r'^configuracoes/',
        include('energiaSolar.apps.configuracoes.urls', namespace='configuracoes')),
    url(r'^orcamentos/',
        include('energiaSolar.apps.orcamentos.urls', namespace='orcamentos')),
    url(r'^fornecedores/',
        include('energiaSolar.apps.fornecedores.urls', namespace='fornecedores'))
]

