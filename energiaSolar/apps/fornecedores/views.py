from django.shortcuts import render
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from .models import *
from django.http import HttpResponse


@method_decorator(login_required, name='dispatch')
class FornecedorView(SearchCustomView):
    """
    View para consulta de fornecedores
    """
    form_class = SearchFornecedor
    model = Fornecedor
    order_field = "nome"
    template_name = "fornecedores/index.html"
    page_title = "Fornecedores"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("fornecedores:fornecedor_index"), "Fornecedores")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(FornecedorView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class FornecedorDetailView(CustomDetailView):
    """
    View para exibição dos detalhes do produto
    """
    model = Fornecedor
    template_name = "fornecedores/detail.html"
    page_title = "Detalhes do Fornecedor"

    def get_context_data(self, **kwargs):
        context = super(FornecedorDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("fornecedores:fornecedor_index"), "Fornecedores"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("fornecedores.add_fornecedor", raise_exception=True)
def fornecedor_create(request):
    """View para Criar um fornecedor"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("fornecedores:fornecedor_index"), "Fornecedores"),
        (reverse("fornecedores:fornecedor_create"), "Novo Fornecedor")
    ]

    form = FornecedorCreateForm()
    if request.method == 'POST':
        form = FornecedorCreateForm(request.POST, request.FILES)
        ultimo_fornecedor = ''
        try:
            ultimo_fornecedor = Fornecedor.objects.latest('codigo')
        except:
            codigo = 0

        if not ultimo_fornecedor:
            codigo = 100
        else:
            codigo = (int(ultimo_fornecedor.codigo) + 100)
        if form.is_valid():
            fornecedor = form.save(commit=False)
            fornecedor.codigo = str(codigo)
            fornecedor.save()

            messages.success(request, "Fornecedor cadastrado com sucesso")
            return redirect(reverse("fornecedores:fornecedor_show", args=[fornecedor.pk]))

    return render(request, "fornecedores/cadastro.html", locals())

@login_required
@permission_required("fornecedores.add_fornecedor", raise_exception=True)
def fornecedor_edit(request, pk):
    """"View para Editar os fornecedores"""
    fornecedor = get_object_or_404(Fornecedor, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("fornecedores:fornecedor_index"), "Fornecedores"),
        (reverse("fornecedores:fornecedor_show", args=[pk]), fornecedor.id),
        ("#", "Editar")
    ]

    form = FornecedorCreateForm(instance=fornecedor)

    if request.method == 'POST':
        form = FornecedorCreateForm(request.POST, request.FILES, instance=fornecedor)

        if form.is_valid():
            fornecedor = form.save(commit=False)
            fornecedor.save()

            messages.success(request, "Fornecedor alterado com sucesso.")
            return redirect(reverse("fornecedores:fornecedor_show", args=[fornecedor.pk]))

    return render(request, "fornecedores/editar.html", locals())

@login_required
@permission_required("fornecedores.add_fornecedor", raise_exception=True)
def fornecedor_delete(request, pk):
    """
    View responsável por deletar um fornecedor
    """
    fornecedor = get_object_or_404(Fornecedor, pk=pk)

    fornecedor.delete()
    messages.success(request, "Fornecedor deletado com sucesso!")
    return HttpResponse("Fornecedor deletado com sucesso!")