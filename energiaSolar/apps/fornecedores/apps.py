from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu


class FornecedoresConfig(AppConfig):
    name = 'energiaSolar.apps.fornecedores'

    menu = {
        'verbose_name': 'Forncedores',
        'permissions': ['fornecedores.add_fornecedor'],
        'login_required': 'True',
        'icon': 'truck-delivery',
        'items': [
            {
                'verbose_name': 'Pesquisar Forncedor',
                'link': reverse_lazy('fornecedores:fornecedor_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Fornecedor',
                'link': reverse_lazy('fornecedores:fornecedor_create'),
                'permissions': [],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)