from django.db import models

class Fornecedor(models.Model):
    codigo = models.CharField(max_length=10, null=True, blank=True)
    nome = models.CharField(max_length=90, null=True, blank=True)
    logradouro = models.CharField(max_length=90, null=True, blank=True)
    numero = models.CharField(max_length=10, null=True, blank=True)
    complemento = models.CharField(max_length=50, null=True, blank=True)
    cep = models.CharField(max_length=12, null=True, blank=True)
    bairro = models.CharField(max_length=60, null=True, blank=True)
    cidade = models.CharField(max_length=90, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)
    telefone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    contato = models.CharField(max_length=90, null=True, blank=True)
    observacoes = models.TextField(max_length=8192, null=True, blank=True)
    tipo_produto = models.CharField(max_length=90, null=True, blank=True)

    def __str__(self):
        return self.nome