from django import forms
from localflavor.br.forms import BRStateChoiceField
from .models import *

class SearchFornecedor(forms.Form):
    """Formulário para consulta de fornecedores da view que lista fornecedores
    de forma genérica"""

    nome = forms.CharField(max_length=90, required=False)
    email = forms.CharField(max_length=90, required=False)
    contato = forms.CharField(max_length=90, required=False)

class FornecedorCreateForm(forms.ModelForm):
    """Formulário para criação de ocorrências"""
    uf = BRStateChoiceField(label="UF")
    cep = forms.CharField(required=True, label="CEP")
    numero = forms.CharField(required=True, label="Número")
    contato = forms.CharField(required=True, label="Contato")
    email = forms.CharField(required=True, label="Email")
    nome = forms.CharField(required=True, label="Nome")
    telefone = forms.CharField(required=True, label="Telefone")

    class Meta:
        model = Fornecedor
        fields = ('__all__')
        labels = {
            'observacoes' : 'Informações Adicionais'
        }