from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.FornecedorView.as_view(), name="fornecedor_index"),
    url(r"^fornecedor/detalhes/(?P<pk>[0-9]+)/$", views.FornecedorDetailView.as_view(), name="fornecedor_show"),
    url(r"^fornecedor/cadastrar/$",views.fornecedor_create, name="fornecedor_create"),
    url(r"^fornecedor/(?P<pk>[0-9]+)/editar/$",views.fornecedor_edit, name="fornecedor_edit"),
    url(r"^fornecedor/(?P<pk>[0-9]+)/excluir/$",views.fornecedor_delete, name="fornecedor_delete")
]