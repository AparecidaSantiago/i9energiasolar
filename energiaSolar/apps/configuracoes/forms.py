# -*- coding: utf-8 -*-
from django import forms
from .models import  Regiao, IncidenciaMes, TipoTarifa, Tarifas
from energiaSolar.apps.produtos.models import *

TIPO_AJUSTE = (
    ('Aumento', 'Aumento'),
    ('Redução', 'Redução'),
)

MESES = (
    ('Janeiro', 'Janeiro'),
    ('Fevereiro', 'Fevereiro'),
    ('Março', 'Março'),
    ('Abril', 'Abril'),
    ('Maio', 'Maio'),
    ('Junho', 'Junho'),
    ('Julho', 'Julho'),
    ('Agosto', 'Agosto'),
    ('Setembro', 'Setembro'),
    ('Outubro', 'Outubro'),
    ('Novembro', 'Novembro'),
    ('Dezembro', 'Dezembro')
)

class SearchTabela(forms.Form):
    """Formulário para consulta das tabelas da view que lista  tabelas de sol
    de forma genérica"""

    regiao = forms.CharField(max_length=90, required=False)

class SearchTarifas(forms.Form):
    """Formulário para consulta das tarifas"""

    valor = forms.DecimalField(required=False)
    tipo = forms.ModelChoiceField(queryset=TipoTarifa.objects.all(), required=False)

class IncidenciaCreateForm(forms.ModelForm):
    """Formulário para importação dos dados da incidência solar"""

    regiao = forms.ModelChoiceField(queryset=Regiao.objects.all(), required=True)

    class Meta:
        model = IncidenciaMes
        fields = ("__all__")

class IncidenciaEditForm(forms.ModelForm):
    """Formulário para importação dos dados da incidência solar"""

    regiao = forms.ModelChoiceField(queryset=Regiao.objects.all(), required=False)

    class Meta:
        model = IncidenciaMes
        exclude = ['regiao']

class ReajusteCreateForm(forms.ModelForm):
    """Formulário para reajustar valores"""

    percentual = forms.FloatField(required=True, widget=forms.TextInput(
                    attrs={'type': 'number','id':'percentual','min': '0','max':'10','step':'0.01'}))
    tipo_ajuste = forms.ChoiceField(label="Tipo do Ajuste", choices=TIPO_AJUSTE, required=True)

    class Meta:
        model = Produto
        exclude = '__all__'

class TarifasCreateForm(forms.ModelForm):
    """Formulário criação da tarifa de energia"""

    valor = forms.DecimalField(required=True)
    tipo = forms.ModelChoiceField(queryset=TipoTarifa.objects.all().order_by('descricao'), required=True)

    class Meta:
        model = Tarifas
        fields = ("__all__")