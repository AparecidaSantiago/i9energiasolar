from django.db import models

class Regiao(models.Model):
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class TipoTarifa(models.Model):
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class IncidenciaMes(models.Model):
    descricao = models.CharField(max_length=20, null=True, blank=True)
    valor = models.CharField(max_length=5, null=True, blank=True)
    regiao = models.ForeignKey(Regiao, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.descricao

class IncidenciaSolar(models.Model):
    regiao = models.ForeignKey(Regiao, null=True, blank=True, on_delete=models.CASCADE)
    media = models.CharField(max_length=5, null=True, blank=True)

    def __int__(self):
        return self.regiao

class Tarifas(models.Model):
    valor = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    tipo = models.ForeignKey(TipoTarifa, null=True, blank=True, on_delete=models.CASCADE)

    def __float__(self):
        return self.valor