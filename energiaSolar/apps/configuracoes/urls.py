from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.TabelaSolarView.as_view(), name="configuracoes_index"),
    url(r"^tabela/cadastrar/$",views.tabela_create, name="tabela_create"),
    url(r"^tabela/detalhes/(?P<pk>[0-9]+)/$",views.tabela_detalhes, name="tabela_show"),
    url(r"^tabela/editar/(?P<pk>[0-9]+)/$",views.tabela_edit, name="tabela_edit"),
    url(r"^tabela/(?P<pk>[0-9]+)/excluir/$", views.tabela_delete, name="tabela_delete"),
    url(r"^reajuste/$", views.reajustar_valor, name="reajustar_valor"),
    url(r"^tarifas/$", views.TarifasView.as_view(), name="tarifas_index"),
    url(r"^tarifas/cadastrar/$", views.tarifa_energia, name="tarifa_energia"),
    url(r"^tarifas/detalhes/(?P<pk>[0-9]+)/$", views.TarifasDetailView.as_view(), name="tarifas_show"),
    url(r"^tarifas/editar/(?P<pk>[0-9]+)/$", views.tarifas_edit, name="tarifas_edit"),
    url(r"^tarifas/(?P<pk>[0-9]+)/excluir/$", views.tarifa_delete, name="tarifas_delete"),

]
