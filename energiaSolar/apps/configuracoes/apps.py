# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse_lazy
from ..core.menu import Menu
from django.apps import AppConfig


class ConfiguracoesConfig(AppConfig):
    name = 'energiaSolar.apps.configuracoes'

    menu = {
        'verbose_name': 'Configurações',
        'permissions': ['configuracoes.add_configuracoes'],
        'login_required': 'True',
        'icon': 'wrench',
        'items': [
            {
                'verbose_name': 'Tabelas de Sol',
                'link': reverse_lazy('configuracoes:configuracoes_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Reajuste de Produtos',
                'link': reverse_lazy('configuracoes:reajustar_valor'),
                'permissions': [],
            },
            {
                'verbose_name': 'Tarifas',
                'link': reverse_lazy('configuracoes:tarifas_index'),
                'permissions': [],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)