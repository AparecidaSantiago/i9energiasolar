from django.shortcuts import render
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from .forms import *
from energiaSolar.apps.produtos.models import Produto, Item
from .models import IncidenciaSolar, IncidenciaMes, Regiao, Tarifas
from ..core.generic_views import SearchCustomView,CustomDetailView
from django.http import HttpResponse

@method_decorator(login_required, name='dispatch')
class TabelaSolarView(SearchCustomView):
    """
    View para consulta de clientes
    """
    form_class = SearchTabela
    model = IncidenciaSolar
    order_field = "regiao"
    template_name = "configuracoes/index.html"
    page_title = "Configuracões"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:configuracoes_index"), "Configurações")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(TabelaSolarView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class TarifasView(SearchCustomView):
    """
    View para consulta de tarifas
    """
    form_class = SearchTarifas
    model = Tarifas
    order_field = "tipo"
    template_name = "configuracoes/tarifas_index.html"
    page_title = "Configuracões"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tarifas_index"), "Tarifas")
    ]

    def get_queryset(self):
        """
        Altera o filtro da tarifa ::
        """
        queryset = super(TarifasView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class TarifasDetailView(CustomDetailView):
    """
    View para exibição dos detalhes das tarifas
    """
    model = Tarifas
    template_name = "configuracoes/tarifas_detail.html"
    page_title = "Tarifas"

    def get_context_data(self, **kwargs):
        context = super(TarifasDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("configuracoes:tarifas_index"), "Tarifas"),
            ("#", self.get_object().id)
        ]

        return context


def tabela_detalhes(request, pk):
    """
    View para exibição dos detalhes das tabelas de energia solar
    """
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tabela_create"), "Configurações"),
        (reverse("configuracoes:configuracoes_create"), "Configurações")
    ]

    tabela = IncidenciaMes.objects.filter(regiao=pk)
    incidencia_solar = get_object_or_404(IncidenciaSolar, regiao=pk)

    return render(request, "configuracoes/detail.html", locals())

@login_required()
def tabela_create(request):
    """
    View para importação do CSV com os dados da tabela de sol
    """
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tabela_create"), "Tabelas de Sol"),
        (reverse("configuracoes:configuracoes_create"), "Tabelas de Sol")
    ]
    meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
             'Jul','Ago', 'Set', 'Out', 'Nov', 'Dez']

    form = IncidenciaCreateForm()

    if request.method == 'POST':
        form = IncidenciaCreateForm(request.POST)

        if form.is_valid():
            tabela = form.save(commit=False)

            valores = request.POST.getlist('valores[]')
            regiao = form['regiao'].value()

            regiao = get_object_or_404(Regiao, pk=regiao)

            if valores:
                for valor, mes in zip(valores, meses):
                    tabela = IncidenciaMes.objects.get_or_create(descricao=mes, regiao=regiao)[0]
                    tabela.valor = valor
                    tabela.save()

                incidencia_mes = IncidenciaMes.objects.filter(regiao=regiao)

                soma = 0
                for i in incidencia_mes:
                    soma += int(i.valor)

                incidencia_solar = IncidenciaSolar.objects.get_or_create(regiao=regiao)[0]

                if incidencia_solar:
                    incidencia_solar.media = int(soma / 12)
                    incidencia_solar.save()

            messages.success(request, "Configurações cadastradas com sucesso")
            return redirect(reverse("configuracoes:tabela_show", args=[regiao.pk]))

    return render(request, "configuracoes/form.html", locals())

@login_required()
def tabela_edit(request, pk):
    """
    View para importação do CSV com os dados da tabela de sol
    """
    tabela = IncidenciaMes.objects.filter(regiao_id=pk)
    regiao = get_object_or_404(Regiao, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tabela_create"), "Tabelas de Sol"),
        (reverse("configuracoes:configuracoes_create"), "Tabelas de Sol")
    ]
    meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
             'Jul','Ago', 'Set', 'Out', 'Nov', 'Dez']

    form = IncidenciaEditForm(instance=regiao)

    if request.method == 'POST':
        form = IncidenciaEditForm(request.POST, instance=regiao)

        if form.is_valid():
            tabela = form.save(commit=False)
            valores = request.POST.getlist('valores[]')

            if valores:
                for valor, mes in zip(valores, meses):
                    tabela = IncidenciaMes.objects.get_or_create(descricao=mes, regiao=regiao)[0]
                    tabela.valor = valor
                    tabela.save()

                incidencia_mes = IncidenciaMes.objects.filter(regiao=regiao)

                soma = 0
                for i in incidencia_mes:
                    soma += int(i.valor)

                incidencia_solar = IncidenciaSolar.objects.get_or_create(regiao=regiao)[0]

                if incidencia_solar:
                    incidencia_solar.media = int(soma / 12)
                    incidencia_solar.save()

        messages.success(request, "Configurações editadas com sucesso")
        return redirect(reverse("configuracoes:tabela_show", args=[regiao.pk]))

    return render(request, "configuracoes/form_edit_tabela.html", locals(), {"form" : form})

@login_required
def reajustar_valor(request):
    """View reajustar os valores dos produtos"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:reajustar_valor"), "Reajustar Valor dos Produtos")
    ]

    form = ReajusteCreateForm()
    if request.method == 'POST':
        form = ReajusteCreateForm(request.POST)

        if form.is_valid():
            produto = form.save(commit=False)
            lista_produtos = Produto.objects.all()
            percentual = float(form.cleaned_data['percentual'])

            for produto in lista_produtos:
                valor_produto = float(produto.valor)
                valor_reajustado = ((valor_produto * percentual) / 100)

                if form.cleaned_data['tipo_ajuste'] == 'Aumento':
                    produto.valor = float(produto.valor) + valor_reajustado

                elif form.cleaned_data['tipo_ajuste'] == 'Redução':
                    produto.valor = float(produto.valor) - valor_reajustado

                if produto.porcentagem_estrutura_produto and produto.porcentagem_servico_produto:
                    produto.valor_servico = (produto.porcentagem_servico_produto * produto.valor) / 100
                    produto.valor_est_metalica = (produto.porcentagem_estrutura_produto * produto.valor) / 100
                    produto.valor_individual_metalica = int(produto.valor_est_metalica / produto.estrutura_metalica)

                #Calcula o percentual de reajuste dos itens do produto se houver
                itens = Item.objects.filter(produto=produto.pk)

                for item in itens:
                    valor_item_reajustado = (produto.valor * item.porcentagem_valor_produto) / 100

                    item.valor_item = float(valor_item_reajustado)
                    item.valor_unitario = (item.valor_item / int(item.quantidade))

                    item.save()

                produto.save()

            messages.success(request, "Reajuste realizado com sucesso")
            return redirect(reverse("produtos:produto_index"))

    return render(request, "configuracoes/reajustar_index.html", locals())

def tarifa_energia(request):
    """
    View para editar a tarifa de energia
    """
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tarifa_energia"), "Tarifas"),
    ]

    form = TarifasCreateForm()
    if request.method == 'POST':
        form = TarifasCreateForm(request.POST)

        if form.is_valid():
            tarifa = form.save(commit=False)
            tipo = form.cleaned_data['tipo']
            valor = form.cleaned_data['valor']
            tipo_tarifa = TipoTarifa.objects.filter(pk=tipo.pk)[0]
            tarifa = Tarifas.objects.get_or_create(tipo=tipo_tarifa)[0]
            tarifa.valor = valor
            tarifa.save()

            messages.success(request, "Tarifa cadastrada com sucesso.")
            return redirect(reverse("configuracoes:tarifas_index"))

    return render(request, "configuracoes/tarifa_create.html", locals())

@login_required
@permission_required("configuracoes.add_tarifas", raise_exception=True)
def tarifas_edit(request, pk):
    """"View para Editar os Clientes"""
    tarifa = get_object_or_404(Tarifas, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("configuracoes:tarifas_index"), "Tarifas"),
        (reverse("configuracoes:tarifas_show", args=[pk]), tarifa.id),
        ("#", "Editar")
    ]


    form = TarifasCreateForm(instance=tarifa)

    if request.method == 'POST':
        form = TarifasCreateForm(request.POST, request.FILES, instance=tarifa)

        if form.is_valid():
            tarifa = form.save(commit=False)
            tarifa.save()

            messages.success(request, "Tarifa alterada com sucesso.")
            return redirect(reverse("configuracoes:tarifas_show", args=[tarifa.pk]))

    return render(request, "configuracoes/tarifas_edit.html", locals())

@login_required
@permission_required("configuracoes.add_tabela", raise_exception=True)
def tabela_delete(request, pk):
    """
    View responsável por deletar um cliente
    """
    tabela = IncidenciaMes.objects.filter(regiao=pk)
    incidencia_solar = IncidenciaSolar.objects.filter(regiao=pk)

    for i in tabela:
        i.delete()
    incidencia_solar.delete()

    messages.success(request, "Tabela deletada com sucesso!")
    return HttpResponse("Tabela deletado com sucesso!")

@login_required
@permission_required("configuracoes.add_tarifa", raise_exception=True)
def tarifa_delete(request, pk):
    """
     View responsável por deletar um cliente
     """
    tarifa = get_object_or_404(Tarifas, pk=pk)

    tarifa.delete()
    messages.success(request, "Tarifa deletada com sucesso!")
    return HttpResponse("Tarifa deletada com sucesso!")