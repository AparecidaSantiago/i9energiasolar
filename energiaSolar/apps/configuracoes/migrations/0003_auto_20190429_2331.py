# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-29 23:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('configuracoes', '0002_auto_20190429_0058'),
    ]

    operations = [
        migrations.CreateModel(
            name='IncidenciaMes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', models.CharField(blank=True, max_length=20, null=True)),
                ('valor', models.CharField(blank=True, max_length=5, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='incidenciasolar',
            name='mes',
        ),
        migrations.RemoveField(
            model_name='incidenciasolar',
            name='valor',
        ),
        migrations.DeleteModel(
            name='Meses',
        ),
        migrations.AddField(
            model_name='incidenciasolar',
            name='incidencia',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='configuracoes.IncidenciaMes'),
        ),
    ]
