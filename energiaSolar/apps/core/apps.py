from django.apps import AppConfig

class CoreConfig(AppConfig):
    name = 'energiaSolar.apps.core'
