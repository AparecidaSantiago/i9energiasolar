# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Regiao(models.Model):
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class CustomUser(models.Model):
    telefone = models.CharField(max_length=100, null=True, blank=True)
    usuario = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name="usuario")

    def __str__(self):
        return self.telefone

class Configuracoes(models.Model):
    mes = models.CharField(max_length=90, null=True, blank=True)
    valor = models.CharField(max_length=90, null=True, blank=True)
    regiao = models.ForeignKey(Regiao, null=True, blank=True, on_delete=models.CASCADE)
    media = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.mes