# -*- coding: utf-8 -*-
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from django.contrib.auth.models import User
from django.http import HttpResponse
from .models import Configuracoes
from django.contrib.auth.hashers import make_password

@method_decorator(login_required, name='dispatch')
class UsuarioView(SearchCustomView):
    """
    View para consulta de ocorrências
    """
    form_class = SearchUsuario
    model = User
    order_field = "username"
    template_name = "usuarios/index.html"
    page_title = "Usuários"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("usuarios:usuario_index"), "Usuários")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(UsuarioView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class UsuarioDetailView(CustomDetailView):
    """
    View para exibição dos detalhes do usuario
    """
    model = User
    template_name = "usuarios/detail.html"
    page_title = "Usuário"

    def get_context_data(self, **kwargs):
        context = super(UsuarioDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("administracao:usuario_index"), "Usuários"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("usuarios.add_usuario", raise_exception=True)
def usuario_create(request):
    """View para Criar um usuário"""

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("administracao:usuario_index"), "Usuários"),
        (reverse("administracao:usuario_create"), "Novo Usuário")
    ]

    form = UsuarioCreateForm()
    form_custom = UsuarioCustomCreateForm()

    if request.method == 'POST':
        form = UsuarioCreateForm(request.POST)
        form_custom = UsuarioCustomCreateForm(request.POST)
        superuser = form['is_superuser'].value()
        username = form['username'].value()
        email = form['email'].value()
        senha = form['password'].value()

        if form.is_valid():
            usuario = form.save(commit=False)
            senha_crypt = make_password(senha)
            usuario.password = senha_crypt
            usuario.save()

            if form_custom.is_valid():
                custom = form_custom.save(commit=False)
                telefone = form_custom['telefone'].value()
                custom.telefone = telefone
                custom.usuario = usuario
                custom.save()

            messages.success(request, "Usuário cadastrado com sucesso")
            return redirect(reverse("administracao:usuario_show", args=[usuario.pk]))

    return render(request, "usuarios/cadastro.html", locals())

@login_required
@permission_required("usuarios.add_usuario", raise_exception=True)
def usuario_edit(request, pk):
    """"View para Editar os Clientes"""
    usuario = get_object_or_404(User, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("administracao:usuario_index"), "Clientes"),
        (reverse("administracao:usuario_show", args=[pk]), usuario.id),
        ("#", "Editar")
    ]


    form = UsuarioCreateForm(instance=usuario)

    if request.method == 'POST':
        form = UsuarioCreateForm(request.POST, request.FILES, instance=usuario)

        if form.is_valid():
            senha = form['password'].value()

            usuario = form.save(commit=False)
            senha_crypt = make_password(senha)
            usuario.password = senha_crypt
            usuario.save()

            messages.success(request, "Usuário alterado com sucesso.")
            return redirect(reverse("administracao:usuario_show", args=[usuario.pk]))

    return render(request, "usuarios/editar.html", locals())

@login_required
@permission_required("usuarios.add_usuario", raise_exception=True)
def usuario_delete(request, pk):
    """
    View responsável por deletar um usuário
    """
    usuario = get_object_or_404(User, pk=pk)

    usuario.delete()
    messages.success(request, "Usuário deletado com sucesso!")
    return HttpResponse("Usuário deletado com sucesso!")
