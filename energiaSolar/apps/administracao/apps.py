# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from ..core.menu import Menu
from django.urls import reverse_lazy


class AdministracaoConfig(AppConfig):
    name = 'energiaSolar.apps.administracao'

    menu = {
        'verbose_name': 'Usuários',
        'permissions': ['usuarios.add_usuario'],
        'login_required': 'True',
        'link': reverse_lazy('administracao:usuario_index'),
        'icon': 'account',
        'items': []
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)

