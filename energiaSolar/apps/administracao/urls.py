from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.UsuarioView.as_view(), name="usuario_index"),
    url(r"^usuario/detalhes/(?P<pk>[0-9]+)/$",
        views.UsuarioDetailView.as_view(), name="usuario_show"),
    url(r"^usuario/cadastrar/$",views.usuario_create, name="usuario_create"),
    url(r"^usuario/(?P<pk>[0-9]+)/excluir/$", views.usuario_delete, name="usuario_delete"),
    url(r"^usuario/(?P<pk>[0-9]+)/editar/$", views.usuario_edit, name="usuario_edit"),
]
