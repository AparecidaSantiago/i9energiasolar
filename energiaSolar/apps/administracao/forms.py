# -*- coding: utf-8 -*-
from django import forms
from datetime import datetime
from django.contrib.auth.models import User
from .models import *

MES_CHOICES = [
    ('Janeiro', "Janeiro"),
    ("Fevereiro", "Fevereiro"),
    ("Março", "Março"),
    ("Abril", "Abril"),
    ("Maio", "Maio"),
    ("Junho", "Junho"),
    ("Agosto", "Agosto"),
    ("Setembro", "Setembro"),
    ("Outubro", "Outubro"),
    ("Novembro", "Novembro"),
    ("Dezembro", "Dezembro"),
]

class SearchUsuario(forms.Form):
    username = forms.CharField(max_length=20, required=False)

class UsuarioCreateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('first_name', 'last_name','username', 'password', 'email', 'is_superuser')
        labels = {
            'first_name': 'Nome',
            'last_name': 'Sobrenome',
            'username': 'Nome de usuário',
            'password': 'Senha',
            'is_superuser': 'Administrador'
        }

class UsuarioCustomCreateForm(forms.ModelForm):
    telefone = forms.CharField(required=True)

    class Meta():
        model = CustomUser
        exclude = ['usuario']

class SearchConfiguracao(forms.Form):

    regiao = forms.ModelChoiceField(queryset=Regiao.objects.all().order_by('descricao'), required=True, label="Região")

class ConfiguracaoCreateForm(forms.ModelForm):
    mes = forms.ChoiceField(choices=MES_CHOICES)

    class Meta():
        model = Configuracoes
        exclude = ['media']
        labels = {

        }