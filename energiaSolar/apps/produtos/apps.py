# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse_lazy
from ..core.menu import Menu
from django.apps import AppConfig


class ProdutosConfig(AppConfig):
    name = 'energiaSolar.apps.produtos'

    menu = {
        'verbose_name': 'Produtos',
        'permissions': ['produtos.add_produto'],
        'login_required': 'True',
        'icon': 'battery-charging-100',
        'items': [
            {
                'verbose_name': 'Pesquisar Produto',
                'link': reverse_lazy('produtos:produto_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Produto',
                'link': reverse_lazy('produtos:produto_create'),
                'permissions': [],
            },
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)
