$('#id_tipo_item').attr('name', 'tipo_item[]');
$('#id_descricao_item').attr('name', 'descricao_item[]');
$('#id_potencia').attr('name', 'potencia[]');
$('#id_quantidade').attr('name', 'quantidade[]');
$('#id_marca').attr('name', 'marca[]');
$('#id_valor_item').attr('name', 'valor_item[]');
$(".ident").css("display", "none");
$('#btn-add').hide();
$('#btn-plus').hide();
$('.componentes-produto').hide();
$('#distribuir').hide();

$('#id_tipo_produto').change(function(){
    tipo_produto = $('#id_tipo_produto').val();

    //se o tipo do produto for diferente de Avulso
     if(tipo_produto == 2){
        $('.componentes-produto').fadeIn("slow");
        $(".ident").css("display", 'none');
        $('#distribuir').show();
     }
     else{
        $('.componentes-produto').fadeOut("slow");
        $('.componentes-produto').hide();
        $('#distribuir').hide();
     }

    //se o tipo de produto for avulso
    if(tipo_produto == 1){
        $('#btn-plus').show();
    }else{
        $('#btn-plus').hide();
        $('#btn-add').hide();
    }
});

$('#btn-plus').click(function() {
    $('#btn-plus').hide();
    $('#btn-add').show();
    $(".ident").css("display", "");
});

$(document).ready(function(){
    $('#btn-add').click(function() {
        if($('.ident').length == 1)
            $('.card-body').append(btn_remove.clone());

        var ident = $('.ident:first').clone();

        ident.find('input').val('');
        ident.insertAfter($('.ident:last'));
    });

    $(document).on('click', '#btn-remove', function(){
        $(this).closest('.ident').remove();

        //Remove botão de exlcuir e seu label se só existir uma linha para item
        if($('.ident').length == 1){
            $('.col-md-1').remove();
           //$('#btn-remove').remove();
            $('#btn-add').remove();
        }
    });
    //Adiciona div e botão de remover dinamicamente
    btn_remove = $("<div class='float-right mt-2 mb-2'><button type='button' class='btn btn-secondary btn-remove'><span class='mdi mdi-delete'></span> Remover</button></div>");

});
