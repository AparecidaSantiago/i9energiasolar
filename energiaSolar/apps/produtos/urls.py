from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.ProdutoView.as_view(), name="produto_index"),
    url(r"^produto/detalhes/(?P<pk>[0-9]+)/$",views.ProdutoDetailView.as_view(), name="produto_show"),
    url(r"^produto/cadastrar/$",views.produto_create, name="produto_create"),
    url(r"^produto/(?P<pk>[0-9]+)/editar/$",views.produto_edit, name="produto_edit"),
    url(r"^produto/(?P<pk>[0-9]+)/excluir/$",views.produto_delete, name="produto_delete"),
    url(r"^itens/$", views.ItemView.as_view(), name="itens_index"),
    url(r"^produto/itens/(?P<pk>[0-9]+)/editar/$",views.itens_edit, name="itens_edit"),
    url(r"^produto/itens/(?P<pk>[0-9]+)/detalhes/$",views.ItemDetailView.as_view(), name="item_show"),
]
