# -*- coding: utf-8 -*-
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from .models import Produto, Item, TipoItem, TipoProduto, Fornecedor
from django.http import HttpResponse
import math

@method_decorator(login_required, name='dispatch')
class ProdutoView(SearchCustomView):
    """
    View para consulta de ocorrências
    """
    form_class = SearchProduto
    model = Produto
    order_field = "codigo"
    template_name = "produtos/index.html"
    page_title = "Produtos"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("produtos:produto_index"), "Produtos")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(ProdutoView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class ProdutoDetailView(CustomDetailView):
    """
    View para exibição dos detalhes do produto
    """
    model = Produto
    template_name = "produtos/detail.html"
    page_title = "Detalhes do Produto"

    def get_context_data(self, **kwargs):
        context = super(ProdutoDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("produtos:produto_index"), "Produtos"),
            ("#", self.get_object().id)
        ]

        context["itens"] = self.get_object().item_set.order_by("descricao_item")

        return context

@method_decorator(login_required, name='dispatch')
class ItemView(SearchCustomView):
    """
    View para consulta de itens
    """
    form_class = SearchItem
    model = Item
    order_field = "descricao_item"
    template_name = "produtos/item_index.html"
    page_title = "Itens"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("produtos:itens_index"), "Itens")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(ItemView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class ItemDetailView(CustomDetailView):
    """
    View para exibição dos detalhes do produto
    """
    model = Item
    template_name = "produtos/item_detail.html"
    page_title = "Detalhes do Item"

    def get_context_data(self, **kwargs):
        context = super(ItemDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("produtos:produto_index"), "Produtos"),
            ("#", self.get_object().id)
        ]
        return context

@login_required
@permission_required("produtos.add_produto", raise_exception=True)
def produto_create(request):
    """View para Criar um produto"""

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("produtos:produto_index"), "Produtos"),
        (reverse("produtos:produto_create"), "Novo Produto")
    ]

    form = ProdutoCreateForm()
    itens = ItensCreateForm()

    if request.method == 'POST':
        form = ProdutoCreateForm(request.POST)
        itens = ItensCreateForm(request.POST)

        if form.is_valid():
            produto = form.save(commit=False)
            tipo_produto = TipoProduto.objects.get(pk=form['tipo_produto'].value())
            fornecedor = Fornecedor.objects.get(pk=form['fornecedor'].value())
            total_porcentagens = 0

            #calcula porcentagem de valor
            if tipo_produto.descricao != "Avulso":
                valor_produto = form.cleaned_data['valor']
                percentual_servico = form.cleaned_data['porcentagem_servico_produto']
                percentual_metalica = form.cleaned_data['porcentagem_estrutura_produto']
                qtd_est_metalica = form.cleaned_data['estrutura_metalica']
                potencia_inversor = form.cleaned_data['potencia_inversor']
                quantidade_inversor = form.cleaned_data['quantidade_inversor']
                marca_inversor = form.cleaned_data['marca_inversor']
                potencia_placa = form.cleaned_data['potencia_placa']
                quantidade_placa = form.cleaned_data['quantidade_placa']
                marca_placa = form.cleaned_data['marca_placa']
                percentual_placas = form.cleaned_data["percentual_placas"]
                percentual_inversores = form.cleaned_data["percentual_inversores"]

                produto.potencia_inversor = potencia_inversor
                produto.quantidade_inversor = quantidade_inversor
                produto.marca_inversor = marca_inversor
                produto.potencia_placa = potencia_placa
                produto.quantidade_placa = quantidade_placa
                produto.marca_placa = marca_placa

                potencia = float(quantidade_placa * potencia_placa)
                potencia_codigo = abs(int(potencia * (-100)))
                produto.codigo = str(str(fornecedor.codigo) + str(potencia_codigo))
                produto.potencia_produto = (potencia / 1000)

                if not (percentual_metalica and percentual_servico and percentual_inversores and percentual_placas):
                    produto.porcentagem_estrutura_produto = 3
                    produto.porcentagem_servico_produto = 2
                    produto.percentual_inversores = 30
                    produto.percentual_placas = 65
                    produto.estrutura_metalica = 1

                    produto.valor_inversores = (30 / 100) * float(valor_produto)
                    produto.valor_placas = (65 / 100) * float(valor_produto)
                    produto.valor_servico = (2 / 100) * float(valor_produto)
                    produto.valor_est_metalica = (3 / 100) * float(valor_produto)
                else:
                    produto.porcentagem_estrutura_produto = percentual_metalica
                    porcentagem_servico_produto = percentual_servico
                    produto.estrutura_metalica = 1
                    produto.percentual_inversores = percentual_inversores
                    produto.percentual_placas = percentual_placas

                    produto.valor_inversores = (percentual_inversores / 100) * float(valor_produto)
                    produto.valor_placas = (percentual_placas / 100) * float(valor_produto)
                    produto.valor_servico = (percentual_servico / 100) * float(valor_produto)
                    produto.valor_est_metalica = (percentual_metalica / 100) * float(valor_produto)

                produto.valor_individual_placas = (float(valor_produto) / quantidade_placa)
                produto.valor_individual_inversor = (float(valor_produto) / quantidade_inversor)

            else:
                produto.codigo = fornecedor.codigo
                produto.valor = form.cleaned_data['valor']

            produto.save()

            if itens.is_valid():
                item = itens.save(commit=False)
                # Cadastra os itens do produto
                tipos = request.POST.getlist('tipo_item[]')
                descricoes = request.POST.getlist('descricao_item[]')
                potencias = request.POST.getlist('potencia[]')
                quantidades = request.POST.getlist('quantidade[]')
                marcas = request.POST.getlist('marca[]')
                valores = request.POST.getlist('valor_item[]')

                try:
                    for tipo, descricao_item, potencia, quantidade, marca, valor in zip(tipos, descricoes,
                                                                                              potencias, quantidades,
                                                                                              marcas, valores):
                        tipo = int(tipo)
                        tipo_item = TipoItem.objects.get(pk=tipo)
                        prod = Produto.objects.get(pk=produto.pk)
                        item_obj = Item()
                        item_obj.descricao_item = descricao_item
                        item_obj.potencia = potencia
                        item_obj.quantidade = quantidade
                        item_obj.marca = marca
                        item_obj.valor_item = valor
                        item_obj.tipo_item = tipo_item
                        item_obj.valor_unitario = item.valor_unitario = (float(valor) / int(quantidade))
                        item_obj.produto = prod
                        item_obj.save()
                except:
                    None

            messages.success(request, "Produto cadastrado com sucesso")
            return redirect(reverse("produtos:produto_show", args=[produto.pk]))

    return render(request, "produtos/cadastro.html", locals())

@login_required
@permission_required("produtos.add_produto", raise_exception=True)
def produto_edit(request, pk):
    """"View para Editar os Produtos"""
    produto = get_object_or_404(Produto, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("produtos:produto_index"), "Produtos"),
        (reverse("produtos:produto_show", args=[pk]), produto.id),
        ("#", "Editar")
    ]


    form = ProdutoEditForm(instance=produto)

    if request.method == 'POST':
        form = ProdutoCreateForm(request.POST,instance=produto)

        if form.is_valid():
            produto = form.save(commit=False)
            tipo_produto = TipoProduto.objects.get(pk=form['tipo_produto'].value())
            fornecedor = Fornecedor.objects.get(pk=form['fornecedor'].value())
            total_porcentagens = 0

            # calcula porcentagem de valor
            if tipo_produto.descricao != "Avulso":
                valor_produto = form.cleaned_data['valor']
                percentual_servico = form.cleaned_data['porcentagem_servico_produto']
                percentual_metalica = form.cleaned_data['porcentagem_estrutura_produto']
                qtd_est_metalica = form.cleaned_data['estrutura_metalica']
                potencia_inversor = form.cleaned_data['potencia_inversor']
                quantidade_inversor = form.cleaned_data['quantidade_inversor']
                marca_inversor = form.cleaned_data['marca_inversor']
                potencia_placa = form.cleaned_data['potencia_placa']
                quantidade_placa = form.cleaned_data['quantidade_placa']
                marca_placa = form.cleaned_data['marca_placa']
                percentual_placas = form.cleaned_data["percentual_placas"]
                percentual_inversores = form.cleaned_data["percentual_inversores"]

                produto.potencia_inversor = potencia_inversor
                produto.quantidade_inversor = quantidade_inversor
                produto.marca_inversor = marca_inversor
                produto.potencia_placa = potencia_placa
                produto.quantidade_placa = quantidade_placa
                produto.marca_placa = marca_placa

                potencia = float(quantidade_placa * potencia_placa)
                potencia_codigo = abs(int(potencia * (-100)))
                produto.codigo = str(str(fornecedor.codigo) + str(potencia_codigo))
                produto.potencia_produto = (potencia / 1000)

                if not (percentual_metalica and percentual_servico and percentual_inversores and percentual_placas):
                    produto.porcentagem_estrutura_produto = 3
                    produto.porcentagem_servico_produto = 2
                    produto.percentual_inversores = 30
                    produto.percentual_placas = 65
                    produto.estrutura_metalica = 1

                    produto.valor_inversores = (30 / 100) * float(valor_produto)
                    produto.valor_placas = (65 / 100) * float(valor_produto)
                    produto.valor_servico = (2 / 100) * float(valor_produto)
                    produto.valor_est_metalica = (3 / 100) * float(valor_produto)
                else:
                    produto.porcentagem_estrutura_produto = percentual_metalica
                    porcentagem_servico_produto = percentual_servico
                    produto.estrutura_metalica = 1
                    produto.percentual_inversores = percentual_inversores
                    produto.percentual_placas = percentual_placas

                    produto.valor_inversores = (percentual_inversores / 100) * float(valor_produto)
                    produto.valor_placas = (percentual_placas / 100) * float(valor_produto)
                    produto.valor_servico = (percentual_servico / 100) * float(valor_produto)
                    produto.valor_est_metalica = (percentual_metalica / 100) * float(valor_produto)

                produto.valor_individual_placas = (float(valor_produto) / quantidade_placa)
                produto.valor_individual_inversor = (float(valor_produto) / quantidade_inversor)

            else:
                produto.codigo = fornecedor.codigo
                produto.valor = form.cleaned_data['valor']

            produto.save()

            messages.success(request, "Produto editado com sucesso")
            return redirect(reverse("produtos:produto_show", args=[produto.pk]))

    return render(request, "produtos/editar.html", locals())

@login_required
@permission_required("produtos.add_produto", raise_exception=True)
def itens_edit(request, pk):
    """View para Criar um produto"""
    item = get_object_or_404(Item, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("produtos:produto_index"), "Produtos"),
        (reverse("produtos:produto_create"), "Novo Produto")
    ]


    form = ItensCreateForm(instance=item)

    if request.method == 'POST':
        form = ItensCreateForm(request.POST, request.FILES, instance=item)

        if form.is_valid():
            item = form.save(commit=False)
            tipo = form['tipo_item'].value()
            descricao = form['descricao_item'].value()
            potencia = form['potencia'].value()
            quantidade = form['quantidade'].value()
            marca = form['marca'].value()
            valor = form['valor_item'].value()


            tipo = int(tipo)
            tipo_item = TipoItem.objects.get(pk=tipo)
            prod = Produto.objects.get(pk=item.produto.pk)
            item = Item.objects.get(pk=pk)

            item.descricao_item = descricao
            item.potencia = potencia
            item.quantidade = quantidade
            item.marca = marca
            item.valor_item = valor
            item.tipo_item = tipo_item
            item.produto = prod
            item.valor_unitario = (float(valor) / int(quantidade))
            item.save()

        messages.success(request, "Item editado o com sucesso")
        return redirect(reverse("produtos:item_show", args=[item.pk]))

    return render(request, "produtos/item_edit.html", locals())


@login_required
@permission_required("produtos.add_produto", raise_exception=True)
def produto_delete(request, pk):
    """
    View responsável por deletar um produto
    """
    produto = get_object_or_404(Produto, pk=pk)
    itens = Item.objects.filter(produto=produto)

    for item in itens:
        item.delete()

    produto.delete()
    messages.success(request, "Produto deletado com sucesso!")
    return HttpResponse("Produto deletado com sucesso!")