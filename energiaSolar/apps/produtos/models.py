# -*- coding: utf-8 -*-
from django.db import models
from ..fornecedores.models import Fornecedor

class TipoProduto(models.Model):
    descricao = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.descricao

class TipoItem(models.Model):
    descricao = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Produto(models.Model):
    fornecedor = models.ForeignKey(Fornecedor, null=True, blank=True, on_delete=models.CASCADE)
    codigo = models.CharField(max_length=20, null=True, blank=True)
    valor = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    tipo_produto = models.ForeignKey(TipoProduto, null=True, blank=True, on_delete=models.CASCADE)
    descricao = models.CharField(max_length=200, null=True, blank=True)
    potencia_produto = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    estrutura_metalica = models.IntegerField(null=True, blank=True)
    valor_servico = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    valor_est_metalica = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    valor_individual_metalica = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    porcentagem_servico_produto = models.IntegerField(null=True, blank=True)
    porcentagem_estrutura_produto = models.IntegerField(null=True, blank=True)
    potencia_inversor = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    quantidade_inversor = models.CharField(max_length=10, null=True, blank=True)
    marca_inversor = models.CharField(max_length=100, null=True, blank=True)
    valor_inversores = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    valor_individual_inversor = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    potencia_placa = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    quantidade_placa = models.CharField(max_length=10, null=True, blank=True)
    marca_placa = models.CharField(max_length=100, null=True, blank=True)
    valor_placas = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    valor_individual_placas = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    percentual_placas = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    percentual_inversores = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)

    class Meta:
        permissions = (
            ("pode_cadastrar_produto", "Pode cadastrar produto"),
            ("pode_alterar_poduto", "Pode editar produto"),
            ("pode_excluir_produto", "Pode excluir produto"),
        )

    def __str__(self):
        return self.descricao

class Item(models.Model):
    tipo_item = models.ForeignKey(TipoItem, null=True, blank=True, on_delete=models.CASCADE)
    potencia = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    quantidade = models.CharField(max_length=10, null=True, blank=True)
    marca = models.CharField(max_length=100, null=True, blank=True)
    descricao_item = models.CharField(max_length=200, null=True, blank=True)
    produto = models.ForeignKey(Produto, null=True, blank=True, on_delete=models.CASCADE)
    valor_item = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    porcentagem_valor_produto = models.IntegerField(null=True, blank=True)
    valor_unitario = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)

    def __str__(self):
        return self.descricao_item