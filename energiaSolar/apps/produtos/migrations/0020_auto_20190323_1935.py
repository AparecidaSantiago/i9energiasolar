# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-03-23 22:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produtos', '0019_auto_20190321_2154'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='estrutura_metalica',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='item',
            name='servico',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
