# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-06-01 15:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('produtos', '0040_auto_20190601_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='inversor',
            name='produto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='produtos.Produto'),
        ),
        migrations.AddField(
            model_name='placas',
            name='produto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='produtos.Produto'),
        ),
    ]
