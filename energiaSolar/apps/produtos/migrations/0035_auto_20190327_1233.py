# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-03-27 12:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produtos', '0034_auto_20190327_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produto',
            name='porcentagem_estrutura_produto',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='produto',
            name='porcentagem_servico_produto',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
