# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-03-26 00:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('produtos', '0031_auto_20190325_2022'),
    ]

    operations = [
        migrations.RenameField(
            model_name='produto',
            old_name='porcetagem_estrutura_produto',
            new_name='porcentagem_estrutura_produto',
        ),
        migrations.RenameField(
            model_name='produto',
            old_name='porcetagem_servico_produto',
            new_name='porcentagem_servico_produto',
        ),
    ]
