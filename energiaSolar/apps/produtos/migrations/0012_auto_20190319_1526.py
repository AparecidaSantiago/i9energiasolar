# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-03-19 15:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produtos', '0011_auto_20190319_1504'),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('potencia', models.IntegerField(blank=True, null=True)),
                ('qtd_placas', models.IntegerField(blank=True, null=True)),
                ('marca_placa', models.CharField(blank=True, max_length=100, null=True)),
                ('qtd_inversor', models.IntegerField(blank=True, null=True)),
                ('marca_inversor', models.CharField(blank=True, max_length=100, null=True)),
                ('descricao', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='produto',
            name='marca_inversor',
        ),
        migrations.RemoveField(
            model_name='produto',
            name='marca_placa',
        ),
        migrations.RemoveField(
            model_name='produto',
            name='potencia',
        ),
        migrations.RemoveField(
            model_name='produto',
            name='qtd_inversor',
        ),
        migrations.RemoveField(
            model_name='produto',
            name='qtd_placas',
        ),
        migrations.AddField(
            model_name='produto',
            name='descricao',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
