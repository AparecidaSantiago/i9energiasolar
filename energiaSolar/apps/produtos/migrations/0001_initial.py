# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-03-12 17:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Produto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(blank=True, max_length=20, null=True)),
                ('potencia', models.CharField(blank=True, max_length=50, null=True)),
                ('qtd_placas', models.IntegerField(blank=True, null=True)),
                ('marca_placa', models.CharField(blank=True, max_length=100, null=True)),
                ('qtd_inversor', models.IntegerField(blank=True, null=True)),
                ('marca_inversor', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'permissions': (('pode_cadastrar_produto', 'Pode cadastrar produto'), ('pode_alterar_poduto', 'Pode editar produto'), ('pode_excluir_produto', 'Pode excluir produto')),
            },
        ),
    ]
