# -*- coding: utf-8 -*-
from django import forms
from datetime import datetime
from .models import *
from localflavor.br.forms import BRStateChoiceField
from django.core.validators import validate_integer

class SearchProduto(forms.Form):
    """Formulário para consulta de produtos da view que lista produtos
    de forma genérica"""

    codigo = forms.CharField(max_length=20, required=False, label="Código")
    tipo_produto = forms.ModelChoiceField(queryset=TipoProduto.objects.all().order_by('descricao'), required=False,
                                          label="Tipo do Produto")
    fornecedor = forms.ModelChoiceField(queryset=Fornecedor.objects.all(), required=False)

class SearchItem(forms.Form):
    """Formulário para consulta de produtos da view que lista produtos
    de forma genérica"""

    descricao = forms.CharField(max_length=20, required=False)

class ProdutoCreateForm(forms.ModelForm):
    """Formulário para criação de produtos"""
    tipo_produto = forms.ModelChoiceField(queryset=TipoProduto.objects.exclude(pk=0).order_by('descricao'), required=True, label="Tipo do Produto")
    fornecedor = forms.ModelChoiceField(queryset=Fornecedor.objects.all(), required=True)
    valor = forms.DecimalField(required=True)
    estrutura_metalica = forms.IntegerField(required=False, label="Quant. de Est. Metálica")
    potencia_inversor = forms.DecimalField(required=False, label="Potência em Kw")
    quantidade_inversor = forms.IntegerField(required=False, label="Quantidade")
    marca_inversor = forms.CharField(required=False, label="Marca")
    potencia_placa = forms.DecimalField(required=False, label="Potência em Wp")
    quantidade_placa = forms.IntegerField(required=False, label="Quantidade")
    marca_placa = forms.CharField(required=False, label="Marca")
    percentual_placas = forms.IntegerField(required=False, label="Porcentagem Placas")
    percentual_inversores = forms.IntegerField(required=False, label="Porcentagem Inversores")

    class Meta:
        fornecedor = forms.ModelChoiceField(queryset=Fornecedor.objects.all())
        model = Produto
        exclude = ['servico', 'valor_servico', 'valor_est_metalica']
        labels = {
            'descricao': 'Descrição',
            'porcentagem_servico_produto': 'Porcentagem do Serviço',
            'porcentagem_estrutura_produto': 'Porcentagem Est. Metálica',
        }

class ItensCreateForm(forms.ModelForm):
    """Formulário para criação de itens"""
    tipo_item = forms.ModelChoiceField(required=False, queryset=TipoItem.objects.all(), label="Tipo do Item")
    potencia = forms.DecimalField(required=False, label="Potência")
    quantidade = forms.IntegerField(required=False, label="Quantidade")
    marca = forms.CharField(required=False, label="Marca")
    valor_item = forms.DecimalField(required=False)

    class Meta:
        model = Item
        exclude = ['produto']
        labels = {
            'descricao_item': 'Descrição do Item'
        }

class ProdutoEditForm(forms.ModelForm):
    """Formulário para criação de produtos"""
    tipo_produto = forms.ModelChoiceField(queryset=TipoProduto.objects.exclude(pk=0).order_by('descricao'), required=True, label="Tipo do Produto")
    fornecedor = forms.ModelChoiceField(queryset=Fornecedor.objects.all(), required=True)
    valor = forms.DecimalField(required=True)
    estrutura_metalica = forms.IntegerField(required=False, label="Quant. de Est. Metálica")
    potencia_inversor = forms.DecimalField(required=False, label="Potência")
    quantidade_inversor = forms.IntegerField(required=False, label="Quantidade")
    marca_inversor = forms.CharField(required=False, label="Marca")
    potencia_placa = forms.DecimalField(required=False, label="Potência")
    quantidade_placa = forms.IntegerField(required=False, label="Quantidade")
    marca_placa = forms.CharField(required=False, label="Marca")

    class Meta:
        fornecedor = forms.ModelChoiceField(queryset=Fornecedor.objects.all())
        model = Produto
        exclude = ['servico', 'valor_servico', 'valor_est_metalica']
        labels = {
            'descricao': 'Descrição',
            'porcentagem_servico_produto': 'Porcentagem do Serviço',
            'porcentagem_estrutura_produto': 'Porcentagem Est. Metálica',
        }