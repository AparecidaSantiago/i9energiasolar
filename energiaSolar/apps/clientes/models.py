# -*- coding: utf-8 -*-
from django.db import models
from ..configuracoes.models import Regiao, TipoTarifa
from django.utils import timezone

class Cliente(models.Model):
    nome = models.CharField(max_length=90, null=True, blank=True)
    logradouro = models.CharField(max_length=90, null=True, blank=True)
    numero = models.CharField(max_length=10, null=True, blank=True)
    complemento = models.CharField(max_length=50, null=True, blank=True)
    cep = models.CharField(max_length=12, null=True, blank=True)
    bairro = models.CharField(max_length=60, null=True, blank=True)
    cidade = models.CharField(max_length=90, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)
    telefone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    cod_uc = models.CharField(max_length=20, null=True, blank=True)
    consumo = models.CharField(max_length=20, null=True, blank=True)
    rede = models.CharField(max_length=20, null=True, blank=True)
    regiao = models.ForeignKey(Regiao, null=True, blank=True, on_delete=models.CASCADE)
    observacoes = models.TextField(max_length=8192, null=True, blank=True)
    tarifa = models.ForeignKey(TipoTarifa, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        permissions = (
            ("pode_cadastrar_cliente", "Pode cadastrar cliente"),
            ("pode_alterar_cliente", "Pode editar cliente"),
            ("pode_excluir_cliente", "Pode excluir cliente"),
        )

    def __str__(self):
        return self.nome