from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.ClienteView.as_view(), name="cliente_index"),
    url(r"^cliente/detalhes/(?P<pk>[0-9]+)/$",views.ClienteDetailView.as_view(), name="cliente_show"),
    url(r"^cliente/cadastrar/$",views.cliente_create, name="cliente_create"),
    url(r"^cliente/(?P<pk>[0-9]+)/editar/$",views.cliente_edit, name="cliente_edit"),
    url(r"^cliente/(?P<pk>[0-9]+)/excluir/$", views.cliente_delete, name="cliente_delete")
]
