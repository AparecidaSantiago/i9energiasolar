# -*- coding: utf-8 -*-
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import SearchCliente, ClienteCreateForm
from .models import Cliente
from ..configuracoes.models import IncidenciaMes, IncidenciaSolar
from django.http import HttpResponse


@method_decorator(login_required, name='dispatch')
class ClienteView(SearchCustomView):
    """
    View para consulta de clientes
    """
    form_class = SearchCliente
    model = Cliente
    order_field = "nome"
    template_name = "clientes/index.html"
    page_title = "Clientes"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("clientes:cliente_index"), "Clientes")
    ]

    def get_queryset(self):
        """
        Altera o filtro do cliente ::
        """
        queryset = super(ClienteView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class ClienteDetailView(CustomDetailView):
    """
    View para exibição dos detalhes do cliente
    """
    model = Cliente
    template_name = "clientes/detail.html"
    page_title = "Cliente"

    def get_context_data(self, **kwargs):
        context = super(ClienteDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("clientes:cliente_index"), "Clientes"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
def cliente_create(request):
    """View para Criar um cliente"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("clientes:cliente_index"), "Clientes"),
        (reverse("clientes:cliente_create"), "Novo Cliente")
    ]

    form = ClienteCreateForm()
    if request.method == 'POST':
        form = ClienteCreateForm(request.POST, request.FILES)

        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()

            messages.success(request, "Cliente cadastrado com sucesso")
            return redirect(reverse("clientes:cliente_show", args=[cliente.pk]))

    return render(request, "clientes/cadastro.html", locals())

@login_required
def cliente_edit(request, pk):
    """"View para Editar os Clientes"""
    cliente = get_object_or_404(Cliente, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("clientes:cliente_index"), "Clientes"),
        (reverse("clientes:cliente_show", args=[pk]), cliente.id),
        ("#", "Editar")
    ]

    form = ClienteCreateForm(instance=cliente)

    if request.method == 'POST':
        form = ClienteCreateForm(request.POST, request.FILES, instance=cliente)

        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()

            messages.success(request, "Cliente alterado com sucesso.")
            return redirect(reverse("clientes:cliente_show", args=[cliente.pk]))

    return render(request, "clientes/editar.html", locals())

@login_required
def cliente_delete(request, pk):
    """
    View responsável por deletar um cliente
    """
    cliente = get_object_or_404(Cliente, pk=pk)

    cliente.delete()
    messages.success(request, "Cliente deletado com sucesso!")
    return HttpResponse("Cliente deletado com sucesso!")