# -*- coding: utf-8 -*-
from django import forms
from datetime import datetime
from .models import Cliente
from ..configuracoes.models import Regiao, TipoTarifa
from localflavor.br.forms import BRStateChoiceField

REDE = (
    ("-------", "-------"),
    ("Monofásica", "Monofásica"),
    ("Trifásica", "Trifásica"),
)

class SearchCliente(forms.Form):
    """Formulário para consulta de clientes da view que lista clientes
    de forma genérica"""

    nome = forms.CharField(max_length=90, required=False)
    cod_uc = forms.CharField(required=False, label="Und. Consumidora")
    regiao = forms.ModelChoiceField(queryset=Regiao.objects.all(), required=False, label="Região")

class ClienteCreateForm(forms.ModelForm):
    """Formulário para criação de ocorrências"""
    uf = BRStateChoiceField(label="UF")
    cep = forms.CharField(required=True, label="CEP")
    numero = forms.CharField(required=True, label="Número")
    cod_uc = forms.CharField(required=True, label="Und. Consumidora")
    consumo = forms.CharField(required=True, label="Consumo Médio")
    rede = forms.ChoiceField(required=True, label="Rede", choices=REDE)
    regiao = forms.ModelChoiceField(queryset=Regiao.objects.all(), required=True, label="Região")
    tarifa = forms.ModelChoiceField(queryset=TipoTarifa.objects.all(), required=True, label="Tarifa")

    class Meta:
        model = Cliente
        fields = ('__all__')
        labels = {
            'observacoes' : 'Informações Adicionais'
        }