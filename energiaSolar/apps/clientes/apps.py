# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse_lazy
from ..core.menu import Menu

from django.apps import AppConfig


class ClientesConfig(AppConfig):
    name = 'energiaSolar.apps.clientes'

    menu = {
        'verbose_name': 'Clientes',
        'permissions': [],
        'login_required': 'True',
        'icon': 'account-card-details',
        'items': [
            {
                'verbose_name': 'Pesquisar Cliente',
                'link': reverse_lazy('clientes:cliente_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Cliente',
                'link': reverse_lazy('clientes:cliente_create'),
                'permissions': [],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)