# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView
from django.core.exceptions import FieldDoesNotExist

class SearchCustomView(ListView):
    """
    View destinada a listagem de objetos de um model com formulário de suporte a
    pesquisa.

    Parâmetros obrigatórios

    form_class: django.forms.Form
    model: django.models.Model
    order_field: str
    """
    form_class = None
    model = None
    paginate_by = 30
    order_field = "id"
    page_title = "Listagem"
    breadcrumbs = []

    def __init__(self, *args, **kwargs):
        super(SearchCustomView, self).__init__(*args, **kwargs)

    def check_form_used(self):
        """
        Check if form is used for search in model queryset
        """
        form_keys = self.form_class().fields.keys()

        for arg in self.request.GET.keys():
            if arg in form_keys:
                return True

        return False

    def get_context_data(self, **kwargs):
        """
        Filter the queryset based on form claned_data
        """

        # create a local context
        context = {}

        # update context with form
        if self.form_class:
            context['form'] = self.form_class()

        if self.page_title:
            context['page_title'] = self.page_title

        if self.breadcrumbs:
            context['breadcrumbs'] = self.breadcrumbs

        # verify if the list is ordered by any field
        order = self.request.GET.get('order', None)

        # Assigns local object list
        object_list = self.get_queryset()

        # Check if form is used
        if self.request.GET and self.check_form_used():
            form = self.form_class(self.request.GET)
            context['form'] = form

            if form.is_valid():
                search_data = [[field, value] for field, value in form.cleaned_data.items() if
                               value is not None and value]
                object_list = self.get_queryset().filter(**dict(search_data))

        # Check request have an order option
        if order:
            try:
                self.model._meta.get_field(order.replace("-", ""))
            except FieldDoesNotExist:
                order = self.order_field
        else:
            order = self.order_field

        object_list = self.get_queryset().order_by(order)

        super_context = super(SearchCustomView, self).get_context_data(object_list=object_list, **kwargs)

        super_context.update(context)
        return super_context
