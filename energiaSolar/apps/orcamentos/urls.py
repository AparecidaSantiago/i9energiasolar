from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r"^$", views.index, name="orcamento_index"),
    url(r"^orcamento/rapido/$",views.orcamento_rapido_create, name="orcamento_rapido_create"),
]
