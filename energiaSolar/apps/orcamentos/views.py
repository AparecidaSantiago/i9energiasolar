# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.http import HttpResponse
from django.contrib import messages
from django.urls import reverse_lazy as reverse
from ..clientes.models import Cliente
from ..produtos.models import Produto, Item
from ..administracao.models import CustomUser
from .forms import *
from ..configuracoes.models import *
import matplotlib.pyplot as plt
from io import BytesIO
from django.template.loader import render_to_string
from weasyprint import HTML
import numpy as np
import os, base64
from io import BytesIO

@login_required
def index(request):
    """
    View responsável pela tela inicial de geração de orçamentos"""

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("orcamentos:orcamento_index"), "Clientes")
    ]

    cliente_form = OrcamentoClienteCreateForm()
    produto_form = OrcamentoProdutoCreateForm()

    if request.method == 'POST':
        cliente_form = OrcamentoClienteCreateForm(request.POST)
        produto_form = OrcamentoProdutoCreateForm(request.POST)

        if cliente_form.is_valid() and produto_form.is_valid():
            cliente_pk = cliente_form['cliente'].value()
            produto_pk = produto_form['produto'].value()

            cliente = get_object_or_404(Cliente, pk=cliente_pk)
            produto = get_object_or_404(Produto, pk=produto_pk)
            produtos = Item.objects.filter(produto=produto_pk)
            custom = CustomUser.objects.get(usuario_id=request.user.pk)

            try:
                area = (int(produto.quantidade_placa) * 2)
            except:
                area = 0

            incidencia_mes = IncidenciaMes.objects.filter(regiao=cliente.regiao)
            valores_incidencia_mes = list()

            for i in incidencia_mes:
                valores_incidencia_mes.append(float(i.valor) * float(produto.potencia_produto))

            incidencia_solar = IncidenciaSolar.objects.filter(regiao=cliente.regiao)[0]
            media = int(incidencia_solar.media) * produto.potencia_produto

            tarifa = get_object_or_404(Tarifas, tipo=cliente.tarifa)

            meses = []
            anos = []
            tarifas = []
            labels = []
            energia_anual = []

            # obtem os valores de tarifas
            valor = float(tarifa.valor)
            tarifas.append(tarifa.valor)
            porcentagem = (float(tarifa.valor) * float(0.08))
            for i in range(1,25):
                valor += porcentagem
                tarifas.append(round(valor,2))
                porcentagem = (float(valor) * float(0.08))

            #obtem a energia gerada anual
            valor = (float(media) * 12)
            energia_anual.append(int(valor))
            porcentagem = (valor * 0.008)
            for i in range(1, 25):
                valor -= porcentagem
                energia_anual.append(round(int(valor), 2))
                porcentagem = (valor * float(0.008))

            #Gera lista com os meses do ano
            meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez', 'Média']

            #obtem a lista com os valores de incidencia solar da regiao
            incidencia_valores = list()
            for i in incidencia_mes:
                incidencia_valores.append(float(i.valor) * float(produto.potencia_produto))
            incidencia_valores.append(float(incidencia_solar.media) * float(produto.potencia_produto))

            #gera os valores de valor economizado
            economia_investimento = list()
            retorno = list()
            valor_investido = float(produto.valor)
            valor = 0

            for tarifa, energia in zip(tarifas, energia_anual):
                economia = float(round(tarifa, 2)) * float(energia)
                economia_investimento.append(round(economia,2))

            valor_retorno = float(produto.valor)
            # economia_investimento é a economia anual
            for i in economia_investimento:
                valor_retorno -= float(i)

                retorno.append(round(-valor_retorno, 2))

            #Gera grafico de incidencia solar
            img_grafico = gera_grafico_incidencia(valores_incidencia_mes, meses)

            #Gera grafico do retorno do investimento
            #gera_grafico_retorno_investimento(economia_investimento)

            # hoje = date.today()
            pdf_in_memory = BytesIO()

            html_string = render_to_string("orcamento.html", {"cliente": cliente, "produto": produto, 'tarifas': tarifas,
                                                            'energia_anual': energia_anual, 'economia' : economia_investimento,
                                                              'retorno' : retorno, 'area':area, 'produtos': produto, 'producao_estimada' : media,
                                                              'img_grafico': img_grafico, "usuario" : request.user, "custom" : custom})
            html = HTML(string=html_string, base_url=request.build_absolute_uri())
            html.write_pdf(target=pdf_in_memory)
            response = HttpResponse(pdf_in_memory.getvalue(), content_type='application/pdf')
            response['Content-Disposition'] = 'inline;filename=%s-%s-%sKw-%s.pdf' % (cliente.cidade, cliente.nome, produto.potencia_produto, produto.valor)
            
            return response

    return render(request, 'form.html', locals())

@login_required
def orcamento_rapido_create(request):
    """
    View responsável pela geração de orçamentos de produtos avulsos"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("orcamentos:orcamento_index"), "Clientes")
    ]

    cliente_form = OrcamentoClienteCreateForm()
    produto_form = OrcamentoRapidoCreateForm()

    if request.method == 'POST':
        cliente_form = OrcamentoClienteCreateForm(request.POST)
        produto_form = OrcamentoRapidoCreateForm(request.POST)

        if cliente_form.is_valid() and produto_form.is_valid():
            cliente_pk = cliente_form['cliente'].value()
            produto_pk = produto_form['produto'].value()

            cliente = get_object_or_404(Cliente, pk=cliente_pk)
            produto = get_object_or_404(Produto, pk=produto_pk)
            itens = Item.objects.filter(produto=produto_pk)

            pdf_in_memory = BytesIO()

            html_string = render_to_string("orcamento_rapido.html",{"cliente": cliente, "produto": produto, "itens" : itens})
            html = HTML(string=html_string, base_url=request.build_absolute_uri())
            html.write_pdf(target=pdf_in_memory)
            response = HttpResponse(pdf_in_memory.getvalue(), content_type='application/pdf')
            response['Content-Disposition'] = 'inline;filename=%s-%s-%s.pdf' % (
            cliente.cidade, cliente.nome, produto.valor)

            return response

    return render(request, 'form_orcamento_rapido.html', locals())

def gera_grafico_incidencia(valores_incidencia_mes, meses):
    plt.rcParams['figure.figsize'] = (8, 3)
    plt.figure(1)
    xs = [i + 0.5 for i, _ in enumerate(valores_incidencia_mes)]
    plt.bar(xs, valores_incidencia_mes, color="#1E90FF")

    # Nome do eixo y
    plt.ylabel("KWh/mês")

    # Título do gráfico
    plt.title("Média da geração anual")
    plt.xticks([i + 0.5 for i, valor in enumerate(valores_incidencia_mes)], meses)

    xlocs, xlabs = plt.xticks()

    for i, v in enumerate(valores_incidencia_mes):
        x = xlocs[i]-0.25
        y = v*0.93
        plt.text(x, y, str(int(v)), {'color':'white','weight':'bold','size': 7})

    buf = BytesIO()
    plt.savefig(buf, format='png')
    img_grafico = base64.b64encode(buf.getvalue()).decode('utf-8').replace('\n', '')
    buf.close()
    plt.close()

    return img_grafico

def gera_grafico_retorno_investimento(retorno):
    N = 25
    plt.figure(2)
    menStd = (2, 3, 4, 1, 2)
    ind = np.arange(N)  # the x locations for the groups
    width = 0.45  # the width of the bars: can also be len(x) sequence
    anos = range(1, 26)
    plt.subplot()
    p1 = plt.bar(ind, retorno, width, color="#1E90FF")

    plt.ylabel('Valores em R$')
    #plt.legend("Retorno do Investimento", loc=1)
    plt.title('Retorno do Investimento')
    plt.xticks(ind, (anos))
    # plt.yticks(np.arange(-50000, 50000, 30500))

    path = '/static/images/barras2.png'
    plt.savefig(path)
    plt.close()