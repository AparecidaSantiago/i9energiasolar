from django import template
from django.contrib.humanize.templatetags.humanize import intcomma

register = template.Library()

@register.filter
def index(List, i):
    return List[int(i)]


def currency(dollars):
    dollars = round(float(dollars), 2)
    retorno = "%s%s" % (intcomma(int(dollars)), (("%0.2f" % dollars)[-3:]).replace('.',','))
    return  retorno

register.filter('currency', currency)
