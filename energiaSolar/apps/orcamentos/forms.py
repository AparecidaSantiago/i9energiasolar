# -*- coding: utf-8 -*-
from django import forms
from ..clientes.models import Cliente
from ..produtos.models import Produto

class OrcamentoClienteCreateForm(forms.ModelForm):
    """Formulário para geração do orçamento"""

    cliente = forms.ModelChoiceField(queryset=Cliente.objects.all(), required=True, label="Cliente")

    class Meta:
        model = Cliente
        fields = ('__all__')

class OrcamentoProdutoCreateForm(forms.ModelForm):
    """Formulário para geração do orçamento"""

    produto = forms.ModelChoiceField(queryset=Produto.objects.filter(tipo_produto=2), required=True, label="Produto")

    class Meta:
        model = Produto
        fields = ('__all__')


class OrcamentoRapidoCreateForm(forms.ModelForm):
    """Formulário para geração do orçamento"""

    produto = forms.ModelChoiceField(queryset=Produto.objects.filter(tipo_produto=1), required=True, label="Produto")

    class Meta:
        model = Produto
        fields = ('__all__')