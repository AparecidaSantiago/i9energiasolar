# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse_lazy
from ..core.menu import Menu

from django.apps import AppConfig


class OrcamentosConfig(AppConfig):
    name = 'energiaSolar.apps.orcamentos'

    menu = {
        'verbose_name': 'Orçamentos',
        'permissions': [],
        'login_required': 'True',
        'icon': 'cash-usd',
        'items': [
            {
                'verbose_name': 'Orçamento Completo',
                'link': reverse_lazy('orcamentos:orcamento_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Orçamento Simples',
                'link': reverse_lazy('orcamentos:orcamento_rapido_create'),
                'permissions': [],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)