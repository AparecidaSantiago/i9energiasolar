from django.apps import AppConfig


class AuthorizationConfig(AppConfig):
    name = 'energiaSolar.apps.authorization'
